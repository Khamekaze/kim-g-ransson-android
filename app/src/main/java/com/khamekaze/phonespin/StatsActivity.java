package com.khamekaze.phonespin;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;


public class StatsActivity extends Activity {

    private Button backButton;

    private TextView lifeTimeSpinsTV, highestMultiTV, highestScoreTV;
    private int lifeTimeSpins, highestMulti, highestScore;

    private SharedPreferences pref;

    private String fontPath = "fonts/ctprolamina.ttf";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            overridePendingTransition(R.anim.fadein, R.anim.fadeout);
            setContentView(R.layout.activity_stats);

            Typeface sf = Typeface.createFromAsset(getAssets(), fontPath);

            pref = getSharedPreferences("Statistics", MODE_PRIVATE);

            lifeTimeSpins = pref.getInt("TotalSpins", 0);
            highestMulti = pref.getInt("Multiplier", 1);
            highestScore = pref.getInt("TotalScore", 0);

            lifeTimeSpinsTV = (TextView) findViewById(R.id.lifeTimeSpins);
            lifeTimeSpinsTV.setTypeface(sf);
            lifeTimeSpinsTV.setText(String.valueOf(lifeTimeSpins));

            highestMultiTV = (TextView) findViewById(R.id.highestMulti);
            highestMultiTV.setTypeface(sf);
            highestMultiTV.setText(String.valueOf(highestMulti));

            highestScoreTV = (TextView) findViewById(R.id.highestScore);
            highestScoreTV.setTypeface(sf);
            highestScoreTV.setText(String.valueOf(highestScore));

            backButton = (Button) findViewById(R.id.backButtonStats);
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goToMenu(backButton);
                }
            });
        }
    }

    public void goToMenu(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
