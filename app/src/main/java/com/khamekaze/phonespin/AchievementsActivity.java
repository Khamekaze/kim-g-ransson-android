package com.khamekaze.phonespin;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import org.w3c.dom.Text;


public class AchievementsActivity extends Activity {

    private Button backButton;

    private ScrollView sc;

    private TextView titleBored, titleSuperBored, titleGodlike, titleSpintastic, titleTellTheWorld;

    private String fontPath = "fonts/ctprolamina.ttf";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        overridePendingTransition(R.anim.whitefadein, R.anim.fadeout);
        setContentView(R.layout.activity_achievements);

        Typeface sf = Typeface.createFromAsset(getAssets(), fontPath);

        sc = (ScrollView) findViewById(R.id.achScrollView);

        titleBored = (TextView) findViewById(R.id.TitleBored);
        titleSuperBored = (TextView) findViewById(R.id.TitleSuperBored);
        titleGodlike = (TextView) findViewById(R.id.TitleGodlike);
        titleTellTheWorld = (TextView) findViewById(R.id.TitleTellTheWorld);
        titleSpintastic = (TextView) findViewById(R.id.TitleSpintastic);

        titleBored.setText("Bored");
        titleBored.setTextSize(30);
        titleBored.setTextColor(Color.parseColor("#FFFFFF"));

        titleSuperBored.setText("What am I doing with my life?");
        titleSuperBored.setTextSize(30);
        titleSuperBored.setTextColor(Color.parseColor("#FFFFFF"));

        titleSpintastic.setText("Spintastic!");
        titleSpintastic.setTextSize(30);
        titleSpintastic.setTextColor(Color.parseColor("#FFFFFF"));

        titleGodlike.setText("GODLIKE");
        titleGodlike.setTextSize(30);
        titleGodlike.setTextColor(Color.parseColor("#FFFFFF"));

        titleTellTheWorld.setText("Tell the world!");
        titleTellTheWorld.setTextSize(30);
        titleTellTheWorld.setTextColor(Color.parseColor("#FFFFFF"));

        titleBored.setTypeface(sf);
        titleSpintastic.setTypeface(sf);
        titleSuperBored.setTypeface(sf);
        titleTellTheWorld.setTypeface(sf);
        titleGodlike.setTypeface(sf);

        backButton = (Button) findViewById(R.id.achBackButton);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainMenu();
            }
        });
    }

    public void goToMainMenu() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
