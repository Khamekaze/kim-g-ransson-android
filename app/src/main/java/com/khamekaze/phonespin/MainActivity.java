package com.khamekaze.phonespin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Environment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Player;
import com.google.android.gms.plus.Plus;

import java.io.File;


public class MainActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private ImageButton playButton, muteButton, exitButton, statsButton, helpButton;

    private MediaPlayer menuMusic;
    private AudioManager am;

    private boolean musicOn;

    private GoogleApiClient mGoogleApiClient;

    private boolean mResolvingConnectionFailure = false;

    private boolean mAutoStartSignInFlow = true;

    // request codes we use when invoking an external activity
    private static final int RC_RESOLVE = 5000;
    private static final int RC_UNUSED = 5001;
    private static final int RC_SIGN_IN = 9001;

    // achievements and scores we're pending to push to the cloud
    // (waiting for the user to sign in, for instance)
    AccomplishmentsOutbox mOutbox = new AccomplishmentsOutbox();


    View.OnClickListener handler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view == playButton) {
                startGame(playButton);
            }
            else if(view == exitButton) {
                finish();
                System.exit(0);
            } else if(view == statsButton) {
                goToStats();

            } else if(view == helpButton) {
                goToHelp(helpButton);
//                startActivityForResult(Games.Achievements.getAchievementsIntent(mGoogleApiClient), 1);
            }
        }
    };

    public void startGame(View v) {
        Intent intent = new Intent(this, PlayActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        menuMusic.stop();
        menuMusic.release();
        menuMusic = null;
        startActivity(intent);
    }

    public void goToHelp(View v) {
        Intent intent = new Intent(this, HelpActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        menuMusic.stop();
        menuMusic.release();
        menuMusic = null;
        startActivity(intent);
    }

    public void goToStats() {
        Intent intent = new Intent(this, StatsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        menuMusic.stop();
        menuMusic.release();
        menuMusic = null;
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if(savedInstanceState == null) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            overridePendingTransition(R.anim.whitefadein, R.anim.fadeout);
            setContentView(R.layout.activity_main);

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN)
                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                .build();

            playButton = (ImageButton) findViewById(R.id.startBtn);
            playButton.setOnClickListener(handler);

            exitButton = (ImageButton) findViewById(R.id.exitBtn);
            exitButton.setOnClickListener(handler);

//            muteButton = (ImageButton) findViewById(R.id.muteBtn);
//            muteButton.setOnClickListener(handler);

            statsButton = (ImageButton) findViewById(R.id.statsBtn);
            statsButton.setOnClickListener(handler);

            helpButton = (ImageButton) findViewById(R.id.helpBtn);
            helpButton.setOnClickListener(handler);
//        }

//        musicOn = true;
//
//        am = (AudioManager) getSystemService(AUDIO_SERVICE);

            menuMusic = MediaPlayer.create(this, R.raw.menu);
            menuMusic.setLooping(true);
            menuMusic.start();

        File folder = new File(Environment.getExternalStorageDirectory() + "/SuperPhoneSpin");
        boolean success = true;
        if(!folder.exists()) {
            success = folder.mkdir();
        }

        mOutbox.loadLocal(this);
    }

    private boolean isSignedIn() {
        return (mGoogleApiClient != null && mGoogleApiClient.isConnected());
    }

//    @Override
    public void onShowLeaderboardsRequested() {
        if (isSignedIn()) {
            startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(mGoogleApiClient),
                    RC_UNUSED);
        } else {
            return;
        }
    }

    void updateLeaderboards(int finalScore) {
        if (mOutbox.mScore < finalScore) {
            mOutbox.mScore = finalScore;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

        // Set the greeting appropriately on main menu
        Player p = Games.Players.getCurrentPlayer(mGoogleApiClient);
        String displayName;
        if (p == null) {
            displayName = "???";
        } else {
            displayName = p.getDisplayName();
        }


        // if we have accomplishments to push, push them
        if (!mOutbox.isEmpty()) {
            pushAccomplishments();
            Toast.makeText(this, getString(R.string.your_progress_will_be_uploaded),
                    Toast.LENGTH_LONG).show();
        }
    }

    void pushAccomplishments() {
        if (!isSignedIn()) {
            // can't push to the cloud, so save locally
            mOutbox.saveLocal(this);
            return;
        }
        if (mOutbox.mGodLikeAchievement) {
            Games.Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_godlike));
            mOutbox.mGodLikeAchievement = false;
        }
        if (mOutbox.mTellTheWorldAchievement) {
            Games.Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_telltheworld));
            mOutbox.mTellTheWorldAchievement = false;
        }
        if (mOutbox.mSpintasticAchievement) {
            Games.Achievements.unlock(mGoogleApiClient, getString(R.string.achievement_spintastic));
            mOutbox.mSpintasticAchievement = false;
        }
        if (mOutbox.mBoredAchievement > 0) {
            Games.Achievements.increment(mGoogleApiClient, getString(R.string.achievement_really_bored),
                    mOutbox.mScore);
            Games.Achievements.increment(mGoogleApiClient, getString(R.string.achievement_bored),
                    mOutbox.mScore);
        }
        if (mOutbox.mScore >= 0) {
            Games.Leaderboards.submitScore(mGoogleApiClient, getString(R.string.leaderboard),
                    mOutbox.mScore);
            mOutbox.mScore = -1;
        }
        mOutbox.saveLocal(this);
    }

//    void checkForAchievements(int requestedScore, int finalScore) {
//        // Check if each condition is met; if so, unlock the corresponding
//        // achievement.
//        if (isPrime(finalScore)) {
//            mOutbox.mPrimeAchievement = true;
//            achievementToast(getString(R.string.achievement_prime_toast_text));
//        }
//        if (requestedScore == 9999) {
//            mOutbox.mArrogantAchievement = true;
//            achievementToast(getString(R.string.achievement_arrogant_toast_text));
//        }
//        if (requestedScore == 0) {
//            mOutbox.mHumbleAchievement = true;
//            achievementToast(getString(R.string.achievement_humble_toast_text));
//        }
//        if (finalScore == 1337) {
//            mOutbox.mLeetAchievement = true;
//            achievementToast(getString(R.string.achievement_leet_toast_text));
//        }
//        mOutbox.mBoredSteps++;
//    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(menuMusic == null) {

        }
        else {
            menuMusic.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(menuMusic == null) {
            menuMusic = MediaPlayer.create(this, R.raw.menu);
            menuMusic.setLooping(true);
            menuMusic.start();
        }
        menuMusic.start();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    class AccomplishmentsOutbox {
        boolean mGodLikeAchievement = false;
        int mBoredAchievement = 0;
        int mReallyBoredAchievement = 0;
        boolean mTellTheWorldAchievement = false;
        boolean mSpintasticAchievement = false;
        int mScore = -1;

        boolean isEmpty() {
            return !mGodLikeAchievement && !mTellTheWorldAchievement && !mSpintasticAchievement &&
                    mBoredAchievement == 0 && mReallyBoredAchievement == 0 &&
                    mScore < 0;
        }

        public void saveLocal(Context ctx) {
            /* TODO: This is left as an exercise. To make it more difficult to cheat,
             * this data should be stored in an encrypted file! And remember not to
             * expose your encryption key (obfuscate it by building it from bits and
             * pieces and/or XORing with another string, for instance). */
        }

        public void loadLocal(Context ctx) {
            /* TODO: This is left as an exercise. Write code here that loads data
             * from the file you wrote in saveLocal(). */
        }
    }
}



//    @Override
//    public void onWinScreenSignInClicked() {
//        mGoogleApiClient.connect();
//    }
//}
