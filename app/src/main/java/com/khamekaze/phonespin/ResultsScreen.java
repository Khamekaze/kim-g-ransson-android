package com.khamekaze.phonespin;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class ResultsScreen extends Activity {

    private TextView resultMultiplier, resultScore, resultSpins;
    private int score, multiplier, spins;
    private Button menuButton, spinAgainButton, shareButton;
    private String fontPath = "fonts/ctprolamina.ttf";

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private int oldScore, oldMulti, oldSpins;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.activity_results_screen);
        Typeface sf = Typeface.createFromAsset(getAssets(), fontPath);
//        Log.d("CREATE FONT", "Font created");

        Bundle extra = getIntent().getExtras();
        if(extra != null) {
            score = extra.getInt("TotalScore");
            multiplier = extra.getInt("Multiplier");
            spins = extra.getInt("TotalSpins");
        }

            resultMultiplier = (TextView) findViewById(R.id.resultMultiplier);
            resultMultiplier.setTextSize(40);
            resultMultiplier.setTextColor(Color.parseColor("#FFFFFF"));
            resultMultiplier.setTypeface(sf);
            resultMultiplier.setText(String.valueOf(multiplier));

            resultSpins = (TextView) findViewById(R.id.resultSpins);
            resultSpins.setTextSize(40);
            resultSpins.setTextColor(Color.parseColor("#FFFFFF"));
            resultSpins.setTypeface(sf);
            resultSpins.setText(String.valueOf(spins));

            resultScore = (TextView) findViewById(R.id.resultScore);
            resultScore.setTextSize(40);
            resultScore.setTextColor(Color.parseColor("#FFFFFF"));
            resultScore.setTypeface(sf);
            resultScore.setText(String.valueOf(score));

            menuButton = (Button) findViewById(R.id.menuButton);
            menuButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goToMenu(menuButton);
                }
            });

            spinAgainButton = (Button) findViewById(R.id.spinAgainButton);
            spinAgainButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    playAgain(spinAgainButton);
                }
            });

            shareButton = (Button) findViewById(R.id.shareButton);
            shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bitmap bitmap = takeScreenshot();
                    saveBitmap(bitmap);

                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_TEXT, "I just scored " + String.valueOf(score) + " points in Super PhoneSpin!");
                    Uri uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SuperPhoneSpin/screenshot" + String.valueOf(spins) + ".png"));
                    i.putExtra(Intent.EXTRA_STREAM, uri);
                    i.setType("image/png");
                    i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(Intent.createChooser(i, "Share using:"));

                }
            });

        prefs = this.getSharedPreferences("Statistics", MODE_PRIVATE);
        editor = prefs.edit();

        oldScore = prefs.getInt("TotalScore", 0);
        oldMulti = prefs.getInt("Multiplier", 1);
        oldSpins = prefs.getInt("TotalSpins", 0);

        readAndWriteStats();
    }

    public void readAndWriteStats() {
        if(score > oldScore) {
            editor.putInt("TotalScore", score);
            editor.commit();
        }

        if(multiplier > oldMulti){
            editor.putInt("Multiplier", multiplier);
            editor.commit();
        }
            oldSpins += spins;
            editor.putInt("TotalSpins", oldSpins);
            editor.commit();
    }

    public void playAgain(View v) {
        Intent playIntent = new Intent(this, PlayActivity.class);
        playIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(playIntent);
        finish();
    }

    public void goToMenu(View v) {
        Intent menuIntent = new Intent(this, MainActivity.class);
        menuIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(menuIntent);
        finish();
    }

    public Bitmap takeScreenshot() {
        View rootView = findViewById(android.R.id.content).getRootView();
        rootView.setDrawingCacheEnabled(true);
        return rootView.getDrawingCache();
    }

    public void saveBitmap(Bitmap bitmap) {
        File imagePath = new File(Environment.getExternalStorageDirectory() + "/SuperPhoneSpin/screenshot" +
                                    String.valueOf(spins) + String.valueOf(score) + String.valueOf(multiplier) + ".png");
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(imagePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch(FileNotFoundException f) {
            Log.e("SCREENSHOT", f.getMessage(), f);
        } catch(IOException i) {
            Log.e("SCREENSHOT", i.getMessage(), i);
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
