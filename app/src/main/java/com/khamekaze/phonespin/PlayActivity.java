package com.khamekaze.phonespin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class PlayActivity extends Activity implements SensorEventListener {
    private boolean running;
    private ImageView spin;
    private ImageView countDown;
    private ImageView swosh;
    private AnimationDrawable frameAnim;
    private AnimationDrawable countDownAnim;
    private AnimationDrawable swoshAnim;

    private MediaPlayer bgMusic;
    private MediaPlayer ding;

    private TextView scoreTW;
    private TextView levelTW;
    private TextView multiplierTW;
    private TextView spins;

    //Score variables
    private int level = 1;
    private int numSpins = 0;
    private int multiplier = 1;
    private int totalScore = 0;
    private int spinGoal = 10;
    private int totalSpins = 0;

    private float currentOrientation = 0f;

    private SensorManager sensorManager;
    private Sensor sensor;

    private String fontPath = "fonts/ctprolamina.ttf";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        Typeface sf = Typeface.createFromAsset(getAssets(), fontPath);
        setContentView(R.layout.activity_play);

        bgMusic = MediaPlayer.create(this, R.raw.theme1);
        bgMusic.setLooping(true);
        bgMusic.start();

        ding = MediaPlayer.create(this, R.raw.dingding);

        sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);

        spins = (TextView) findViewById(R.id.spins);
        spins.setTextSize(50);
        spins.setTypeface(sf);
        spins.setTextColor(Color.parseColor("#FFFFFF"));
        spins.setText("Spins: " + String.valueOf(numSpins));

        scoreTW = (TextView) findViewById(R.id.scoreTW);
        scoreTW.setTextSize(50);
        scoreTW.setTypeface(sf);
        scoreTW.setTextColor(Color.parseColor("#FFFFFF"));
        scoreTW.setText("Score: " + String.valueOf(totalScore));

        levelTW = (TextView) findViewById(R.id.levelTW);
        levelTW.setTextSize(50);
        levelTW.setTypeface(sf);
        levelTW.setTextColor(Color.parseColor("#FFFFFF"));
        levelTW.setText("Level " + String.valueOf(level) + " Goal: " + String.valueOf(spinGoal));

        multiplierTW = (TextView) findViewById(R.id.multiplierTW);
        multiplierTW.setTextSize(50);
        multiplierTW.setTypeface(sf);
        multiplierTW.setTextColor(Color.parseColor("#FFFFFF"));
        multiplierTW.setText("Multiplier: x" + String.valueOf(multiplier));



        running = true;

        spin = (ImageView) findViewById(R.id.spinner);
        countDown = (ImageView) findViewById(R.id.countDown);
        frameAnim = (AnimationDrawable) spin.getDrawable();
        countDownAnim = (AnimationDrawable) countDown.getDrawable();

        swosh = (ImageView) findViewById(R.id.bgAnimation);
        swosh.setPadding(-35, -35, -35, -35);
        swoshAnim = (AnimationDrawable) swosh.getDrawable();

        startAnimation();

    }

    public void goToResults() {
        Intent intent = new Intent(this, ResultsScreen.class);
        if(totalScore > numSpins) {
            intent.putExtra("TotalScore", totalScore + numSpins);
        } else if(totalScore == 0) {
            intent.putExtra("TotalScore", numSpins);
        }
        intent.putExtra("TotalSpins", totalSpins);
        intent.putExtra("Multiplier", multiplier);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        stopAnimations();
        bgMusic.stop();
        bgMusic.release();
        bgMusic = null;
        ding.release();
        ding = null;
        startActivity(intent);
    }

    public void startAnimation() {
        frameAnim.start();
        countDownAnim.start();
        swoshAnim.start();
    }

    public void checkForResults() {
        if(numSpins == spinGoal) {
            ding.start();
            totalScore = numSpins * multiplier;
            level++;
            spinGoal += 1;
            totalSpins += numSpins;
            numSpins = 0;
            if(increaseMultiplier(countDownAnim)) {
                if(multiplier == 1) {
                    multiplier++;
                } else {
                    multiplier += 2;
                }
            }
            countDownAnim.stop();
            countDownAnim.selectDrawable(0);
            countDownAnim.start();
        }

        if(numSpins < spinGoal && countDownAnim.getCurrent() == countDownAnim.getFrame(15)) {
            sensorManager.unregisterListener(this);
            frameAnim.stop();
            countDownAnim.stop();
            goToResults();
        }
    }

    private boolean increaseMultiplier(AnimationDrawable ad) {
        ad.stop();
        if(ad.getCurrent() == countDownAnim.getFrame(15)) {
            return false;
        } else if(ad.getCurrent() == countDownAnim.getFrame(14)) {
            return false;
        } else if(ad.getCurrent() == countDownAnim.getFrame(13)) {
            return false;
        }
        else
            return true;
    }

    public void stopAnimations() {
        frameAnim.stop();
        countDownAnim.stop();
        swoshAnim.stop();
    }



    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        float gyroAcc = Math.round(sensorEvent.values[2]);

        if(gyroAcc > 0 || gyroAcc < 0) {
            currentOrientation += gyroAcc;
        }

        if(currentOrientation >= 1080) {
            currentOrientation = 0;
            numSpins ++;
        } else if(currentOrientation <= -1080) {
            currentOrientation = 0;
            numSpins ++;
        }

        checkForResults();

        levelTW.setText("Level " + String.valueOf(level) + " Goal: " + String.valueOf(spinGoal));
        scoreTW.setText("Score: " + String.valueOf(totalScore));
        spins.setText("Spins\n" + String.valueOf(numSpins));
        multiplierTW.setText("Multiplier\n" + "x" + String.valueOf(multiplier));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
        if(bgMusic == null) {

        }
        else {
            bgMusic.pause();
        }
        stopAnimations();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopAnimations();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(bgMusic == null) {
            bgMusic = MediaPlayer.create(this, R.raw.theme1);
            bgMusic.setLooping(true);
            bgMusic.start();
        } else {
            bgMusic.start();
        }
        startAnimation();
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    public void onBackPressed() {

    }
}
